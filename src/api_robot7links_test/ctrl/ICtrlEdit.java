/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api_robot7links_test.ctrl;

import api_robot7links_test.bean.MyRobot;

/**
 *
 * @author AudergonV01
 */
public interface ICtrlEdit {
    void onSave(MyRobot robot);
}
